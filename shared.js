// Shared code needed by all three pages.
 mapboxgl.accessToken = 'pk.eyJ1IjoiYmVhbm1vbW8iLCJhIjoiY2s4emYwbWE0MDV0ZjNrb2w5d25pZ2ZreSJ9.iEQ26u_uXzAhuOPnV56y_Q';
 
// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];
var currentLocation = []; //[1]:lat [2]:lon [3]:accuracy
// GPS from sensor app    
function getLocation(){
    if(navigator.geolocation){
    navigator.geolocation.watchPosition(getCurrentLocation);
    }else{
        alert("Location denied by user")
    }
}
function getCurrentLocation(position)
    {
        // get the current latitude and longitude:
        currentLocation = [Number(position.coords.latitude).toFixed(4),Number(position.coords.longitude).toFixed(4),Number(position.coords.accuracy).toFixed(2)]
    }

    
//Added run class
class run{
    constructor(name,start,destination){
        this._name = name;
        this._start = start;//should b lnglat obj
        this._destination = destination;//should b lnglat obj
        this._date = Date() ;//event listener
        this._path = [];//should b lnglat obj

    }
}
